package models;
import java.lang.String;
public class Pessoa extends Model {

    private String nome;
    private String anoNascimento;
    private String altura;


    public Pessoa(Long id, String nome, String altura, String anoNascimento) {
        setId(id);
        setNome(nome);
        setAltura(altura);
        setAnoNascimento(anoNascimento);
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getAltura() {
        return altura;
    }

    public void setAltura(String altura) {
        this.altura = altura;
    }

    public String getAnoNascimento() {
        return anoNascimento;
    }

    public void setAnoNascimento(String anoNascimento) {
        this.anoNascimento = anoNascimento;
    }

    @Override
    public String toString() {
        return "Pessoa{" +
                "nome='" + nome + '\'' +
                ", anoNascimento='" + anoNascimento + '\'' +
                ", altura=" + altura +
                '}';
    }
}
